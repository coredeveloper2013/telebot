<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha256-916EbMg70RQy9LHiGkXzG8hSg9EdNy97GazNG/aiY1w=" crossorigin="anonymous" />
</head>
<body>


<div class="container">
    <br><br>

</div>

<div class="container">

    <div class="starter-template">
        @if($php_errormsg)
            <p class="alert alert-danger">
                The bot token you provided is not valid.
            </p>
        @endif
        <p>
        <form method="GET" action="{{route('index')}}">

            <input type="text" name="telegram_token" placeholder="Your bot token" class="form-control">
            <br>
                <ol>
                    <li>Create a bot</li>
                    <li>Add it to your group</li>
                    <li>Give all admin permission to the bot to access the messages of the group</li>
                    <li>Collect <strong>TELEGRAM_BOT_TOKEN</strong> and paste into the above input to test</li>
                    <li>It will show you all current messages which contains url or links</li>
                </ol>
            <br>
            <input type="submit" value="find" class="btn btn-primary">

        </form>
        </p>
        @if(count($link_arr))
            <h3>Collected Links</h3>
                @foreach($link_arr as $data)
                    <div class="well">{{$data}}</div>
                @endforeach

        @else
            <p>
                no links found
            </p>
        @endif

    </div>

</div>


</body>
</html>
